"'Abstract Email module."""
import smtplib
import mimetypes
from email import encoders
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage


class AbstractHTMLEmail(object):
    """Build class for html email."""

    def assemble_and_send_email(self, subject, email_body, to_addresses='', cc_addresses='', bcc_addresses='',
                                image_list=None, alt_text_only=None, attachment_list=None):
        """Assemble method."""
        from_address = 'balajitest25791@outlook.com'
        to_recipients = []
        cc_recipients = []
        bcc_recipients = []
        if to_addresses:
            to_addresses = to_addresses.replace(';', ',').replace(' ', '')
            to_recipients = to_addresses.split(',')
        if cc_addresses:
            cc_addresses = cc_addresses.replace(';', ',').replace(' ', '')
            cc_recipients = cc_addresses.split(',')
        if bcc_addresses:
            bcc_addresses = bcc_addresses.replace(';', ',').replace(' ', '')
            bcc_recipients = bcc_addresses.split(',')
        # Create message container - the correct MIME type is multipart/alternative.
        msg_root = MIMEMultipart("related")
        msg_root['Subject'] = subject
        msg_root['From'] = from_address
        msg_root.preamble = 'This is a multi-part message in MIME format.'
        if to_addresses:
            msg_root['To'] = ", ".join(to_recipients)
        if cc_addresses:
            msg_root['Cc'] = ", ".join(cc_recipients)
        if bcc_addresses:
            msg_root['Bcc'] = ", ".join(bcc_recipients)
        # Encapsulate the plain and HTML versions of the message body in an
        # alternative part, so message agents can decide which they want to display.
        msg_alternative = MIMEMultipart('alternative')
        msg_root.attach(msg_alternative)
        if alt_text_only:
            # alternative_text for the receiver only have options to display texts
            msg_text = MIMEText(alt_text_only)
            # always add first so that it try to render html if it fails it render text
            msg_alternative.attach(msg_text)  
        # assemble the html
        html = self._start_email_body() + email_body + self._complete_email_body()
        msg_text = MIMEText(html, 'html')
        msg_alternative.attach(msg_text)
        # Record the MIME types of both parts - text/plain and text/html.
        # Attach parts into message container.
        # According to RFC 2046, the last part of a multipart message, in this case
        # the HTML message, is best and preferred.
        if image_list:
            self._add_images_to_email(image_list, msg_root)
        if attachment_list:
            self._add_attachments_to_email(attachment_list, msg_root)
        # Send the message via local SMTP server
        # mailserver = smtplib.SMTP('localhost')
        mailserver = smtplib.SMTP("mail.<domain>.com", 25)
        # identify ourselves to smtp gmail client
        mailserver.ehlo()
        # secure our email with tls encryption
        mailserver.starttls()
        # re-identify ourselves as an encrypted connection
        mailserver.ehlo()
        mailserver.login('me@gmail.com', 'mypassword')
        # sendmail function takes three arguments: sender's address, recipient's address
        # and message to send - here it is sent as one string
        # smtp header does not care about to/cc/bcc. just supply address
        try:
            mailserver.sendmail(from_address, to_recipients + cc_recipients + bcc_recipients, msg_root.as_string())
        except smtplib.SMTPRecipientsRefused:
            raise Exception('One or more email addresses must be passed as the intended recipient.')
        mailserver.quit()

    def _start_email_body(self):
        """Create the start of the email body. Override this method to change the body style."""
        html = """‹html><head›<style›‹/style></head><body style='background-color:white;'>
        """
        return html

    def _complete_email_body(self):
        """Complete the end of the email body."""
        html = """</body›‹/html›"""
        return html

    def _add_images_to_email(self, image_list, msg_root):
        """For each image in the image list, generate tag <img src="cid:imageX"> So <img src="cid:image0">,
         <img src="cid:image1">, etc."""
        for idx, image in enumerate(image_list):
            # This example assumes the image is in the current directory
            fp = open(image, 'rb')
            msg_image = MIMEImage(fp.read())
            fp.close()
            # Define the image's ID as referenced above based on the id number of the image
            msg_image.add_header("Content-ID", '<image' + str(idx) + '>')
            msg_root.attach(msg_image)

    def _add_attachments_to_email(self, attachment_list, msg_root):
        """Loop through each attachment tuple, sourcing the raw_filename and the name to be displayed in the email."""
        for _idx, attachment_tuple in enumerate(attachment_list):
            file_to_send, attach_as = attachment_tuple
            ctype, encoding = mimetypes.guess_type(file_to_send)
            if ctype is None or encoding is not None:
                ctype = "application/octet-stream"
            maintype, subtype = ctype.split("/", 1)
            if maintype == "text":
                fp = open(file_to_send)
                # Note: we should handle calculating the charset
                attachment = MIMEText(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype == "image":
                fp = open(file_to_send, "rb")
                attachment = MIMEImage(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype == "audio":
                fp = open(file_to_send, "rb")
                attachment = MIMEAudio(fp.read(), _subtype=subtype)
                fp.close()
            else:
                fp = open(file_to_send, "rb")
                attachment = MIMEBase(maintype, subtype)
                attachment.set_payload(fp.read())
                fp.close()
                encoders.encode_base64(attachment)
        attachment.add_header("Content-Disposition", "attachment", filename=attach_as)
        msg_root.attach(attachment)
